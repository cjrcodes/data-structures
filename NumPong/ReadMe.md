This is NumPong!

Main Class: NumPong.java

Additional Required Classes: Ball.java, Cup.java

---Data Structure: Linked List---

Goal: A matching ball number must be thrown into a matching cup number, for every cup.

Input: The user enters a number between 1 and 200 or allows the program to select a random number.

Implementation: The input value will be the number of cups and balls generated.
Every ball and cup will have a unique value number up to the input value. (Example, Input: 3, Ball 1, Ball 2, Ball 3, Cup 1, Cup 2, Cup 3)
The cups function as an array, in which every element is the head of a linked list (because there can be more than 1 ball for every cup.)
Every round, a random ball will be thrown into a random cup, infinitely, until every cup is filled. Then, any ball that does not match the cup, will be removed from the cup.
The game ends when every cup has a matching ball.

**The gif is sped up due to memory restrictions**

![NumPong Gif](NumPong/NumPong.gif)


