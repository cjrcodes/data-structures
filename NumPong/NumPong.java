/*Program name: Num Pong
 * 
 * Description: Throw an infinite series of balls at cups until each cup is filled. Then, remove any
 * ball that does not match the cup number. When each cup has a matching ball number, the game is over.
 * 
 * Programmer: Christian Rodriguez
 * 
 * Main Class: NumPong.java
 * 
 * Additional Required Classes: Cup.java, Ball.java
 * 

*/
import java.util.*;

//Main
public class NumPong {

	//The value that will determine the number of cups, either selected by the user or randomly
	static int valueSelect;
	
	//Array of balls that will be thrown into the cups
	static Ball [] balls;
	
	//Total amount of balls thrown
	static int ballsThrown = 0;
	
	//Cups in which the balls will be thrown into
	static Cup [] cups;
	
	//Current round number
	static int round = 0;
	
	//Random number
	static Random rand = new Random();
	
	
	public static void main(String [] args) throws InterruptedException{
		//Debug Code
		/*Cup a = new Cup(1);
		Cup b = new Cup(2);
		Cup c = new Cup(3);
		
		Ball ballA = new Ball(1);
		Ball ballB = new Ball(2);
		Ball ballC = new Ball(3);
		Ball ballD = new Ball(1);

		a.append(ballA);
		a.append(ballB);
		a.append(ballC);
		a.append(ballD);

		System.out.println(a.containsValue());
		System.out.println(a.valueSearch(ballB).value);
		Cup.remove(a);
		
		testCups[0] = a;
		
		printCups(testCups);*/
		
		
		//Greetings, handles user input
		greetings();
		
		//Generates the balls and cups
		generateBallsAndCups();
		//printBallsAndCups();
		
		//Throwing phase
		throwingPhase();
		
		//Pruning phase
		pruningPhase();
		
		//When the game is over, print winStatement
		winStatement();
	}
	
	public static void greetings(){
		System.out.println("Welcome to Christian's NumPong! Enter a number between 1-200 or press r for a random number.");
		menuInput();
	}
	
	//Handles user input
	public static int menuInput() {
		Scanner sc = new Scanner(System.in);
	    String input = sc.nextLine();
	    int tryNum = 1;
	    
	    //Try/Catch
	    do{
	    try{
	    	//If input is not a number
	    if(!input.matches("[0-9]+")){
	    	
	    	//Randomly selected number
	    	if(input.equals("R") || input.equals("r")){
	    		int x = rand.nextInt(10) + 1;
	    		
	    		valueSelect = x;
	    		System.out.println("\nRandomly choosen number: " + valueSelect);
	    		tryNum = 2;
	    	    sc.close();
	    		return valueSelect;
	    	}
	    	
	    	else {
	    		System.out.println("\nInput was neither a number nor r. Please select a number or press (r)andom.");
	    		menuInput();
	    	}
	    }
	    
	    else if(Integer.parseInt(input) <= 200 && Integer.parseInt(input) > 0){
	    	valueSelect = Integer.parseInt(input);
	    	System.out.println("\nYour selected number: " + valueSelect);
	    	tryNum = 2;
		    sc.close();
	    	return valueSelect;
	    }
	    
	    else{
	    System.out.println("\nYour input was not valid (Greater than 200 or less than 1). Please select a another number or press (r)andom. ");
	    return menuInput();
	    }
	    
	    return -1;
	    }
	    
	    catch(Exception e){
	    System.out.println("\nYour input was not valid (Greater than 200 or less than 1). Please select a another number or press (r)andom. ");
	    return menuInput();
	    }

	    } while(tryNum == 1);
	    
}
	
		public static void generateBallsAndCups(){
			
			balls = new Ball[valueSelect];
			cups = new Cup[valueSelect];
			for(int i = 0; i < valueSelect; i++){
				balls[i] = new Ball(i + 1);
				cups[i] = new Cup(i + 1);
			}
		}
		
		public static void printBallsAndCups(){
			System.out.println();
			
			System.out.println("Balls\n");
			for(int i = 0; i < balls.length; i++){
				System.out.println("Ball " + (i + 1) + ": " + balls[i].value);
			}
			
			System.out.println("\nCups\n");
			for(int i = 0; i < cups.length; i++){
				System.out.println("Cup " + (i + 1) + ": " + cups[i].value);
			}
		}
		
		public static void throwingPhase() throws InterruptedException{
			
			round++;
			System.out.println("\nRound " + round + "!\n");
			
			//Thread.sleep(100);
			System.out.println("3");
			
			//Thread.sleep(100);
			System.out.println("2");
			
			//Thread.sleep(100);
			System.out.println("1");
			
			//Thread.sleep(100);
			System.out.println("\nGo!");
			
			while(!everyCupFilled()){
			
			//Select a random value
	    	int x = rand.nextInt(valueSelect);
	    	int y = rand.nextInt(valueSelect);

	    	//System.out.println("\t\tCup " + (x + 1));
	    	//System.out.println("\t\tBall " + (y+ 1));

	    	
	    	ballsThrown++;
	    	//Ball gets added to cup
	    	cups[x].append(balls[y]);
	    	
	    	//Thread.sleep(100);
	    	
	    	System.out.println("\nBall " + balls[y].value + " --> Cup " + cups[x].value);
			}
			
			System.out.println("Throwing phase over!\n");
			//Thread.sleep(100);
			printCups(cups);
			//Thread.sleep(100);
			System.out.println("\nPruning phase begins!");
			//Thread.sleep(100);
			pruningPhase();
		}
		
		//Pruning phase
		public static void pruningPhase() throws InterruptedException {
			
			for(int i = 0; i < cups.length; i++) {
				cups[i].prune();
			}
			
			
			//Thread.sleep(100);
			//All cups have matching balls
			if(cupsMatched()) {

			}
			
			//All cups do not have matching balls
			else {
				//Thread.sleep(100);
				printCups(cups);

				System.out.println("\nNext round!");

				//Thread.sleep(100);
				throwingPhase();
			}
			
		}
		
		
		public static void winStatement() throws InterruptedException{
			while(!cupsMatched()){
				
			}
			printCups(cups);

			Thread.sleep(100);
			System.out.println("\nGame over! You threw " + ballsThrown + " balls over " + round + " Rounds!");
			System.out.println("Thank you for playing! ~ CR :)");
		}
		
		//Each cup
		public static boolean cupsMatched() {
			for(int i = 0; i < cups.length; i++) {
				
				if(cups[i].front != null) {
				Ball ptr = cups[i].front;
				
				while(ptr != null) {
					//Cup contains value that does not match
					if(cups[i].value != ptr.value) {
						return false;
					}
					
					else {
						ptr = ptr.next;
						continue;
					}
				}
				
				}
				
				else {
					return false;
				}
			}
			
			return true;
		}
	
		//Is every cup filled
		public static boolean everyCupFilled(){
			for(int i = 0; i < cups.length; i++){
				if(cups[i].front == null){
					return false;
				}
				
				continue;
			}
			
			return true;
		}
		
		public static void printCup(Cup a){
			if(a.front == null){
				System.out.println("Cup " + a.value + ": Empty Cup");
			}
			
			else{
			Ball ptr = a.front;
			while(ptr != null){
				if(ptr == a.front && ptr.next != null){
					System.out.print("Cup " + (a.value + 1) +": " + ptr.value + " --> ");
					ptr = ptr.next;

				}
				
				else if(ptr == a.front && ptr.next == null){
					System.out.print("Cup " + (a.value+1) +": " + ptr.value + "\n");
					ptr = ptr.next;
				}

				
			else if (ptr.next != null){
				System.out.print(ptr.value + " --> ");
				ptr = ptr.next;
				}
				
				else{
					System.out.print(ptr.value +"\n");
					ptr = ptr.next;

				}
			}
			}
		}
		
		public static void printCups(Cup [] a){
			
			
			for(int i = 0; i < cups.length; i++){
			//Empty cup
			if(cups[i].front == null){
				System.out.println("Cup " + cups[i].value + ": Empty Cup");
			}
			
			else{
			Ball ptr = cups[i].front;
			while(ptr != null){
				if(ptr == cups[i].front && ptr.next != null){
					System.out.print("Cup " + (i+1) +": " + ptr.value + " --> ");
					ptr = ptr.next;

				}
				
				else if(ptr == cups[i].front && ptr.next == null){
					System.out.print("Cup " + (i+1) +": " + ptr.value + "\n");
					ptr = ptr.next;
				}

				
			else if (ptr.next != null){
				System.out.print(ptr.value + " --> ");
				ptr = ptr.next;
				}
				
				else{
					System.out.print(ptr.value +"\n");
					ptr = ptr.next;

				}
			}
			}
			
			}
		
}
}

//Thanks for reading my code! ~ CR :)
