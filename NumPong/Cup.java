//Cup, functions similar to a linked list in that each cup can chain/link balls within the cup
public class Cup {

	int value;
	Ball ptr, front, rear;
	
	public Cup(int value){
		this.value = value;
		this.front = this.ptr = this.rear  =  null;
	}
	
	//Add to the cup
	public void append(Ball ball){
		
		Ball b = new Ball(ball.value);
		
		if(this.front == null){
			
			this.front = this.rear = b;
		}
		
		else{
			this.rear.next = b;
			b.prev = this.rear;
			this.rear = b;
			this.rear.next = null;
		}
	}
	
	public int length(){
		this.ptr = this.front;
		int length = 0;
		
		while(this.ptr != null){
			length++;
			this.ptr = this.ptr.next;
		}
		
		return length;
	}
	
	public boolean containsValue(){
		this.ptr = this.front;
		
		while(this.ptr != null){
			if(this.ptr.value == this.value){
				return true;
			}
			
			else{
			this.ptr = this.ptr.next;
			}
		}
		
		return false;
	}
	
	
	public Ball valueSearch(Ball p){
		Ball b = p;
		
		while(b != null){
			if(b.value == this.value){
				return b;
			}
			
			else{
			b = b.next;
			}
		}
		
		return p;
	}
	
	public void prune(){
	
		//Pointer set to front
		this.ptr = this.front;
		//System.out.println("Pruning cup: " + this.value);
		
		//If the cup contains the matching ball value
		if(this.containsValue()){

		while(ptr != null){
		//System.out.println("Current: " + this.ptr.value);
		//This element does not match, remove it
		if(this.ptr.value != this.value){
			remove(this, this.ptr);
			this.ptr = this.ptr.next;
		}
		
		//Match found
		else{
			//Front is not equal to value, but pointer is. Make pointer front.
			if(this.front.value != this.value && this.ptr.value == this.value){
				this.front = this.ptr;
				this.ptr = this.ptr.next;
			}
			
			//Match found, keep moving
			else{
			this.ptr = ptr.next;
		}
	
		}
		}
		}
		
		
		
		else{
				//System.out.println("Cup emptied " + this.value);
				this.front = null;
		}
	}
	//Remove a ball from the cup
	public void remove(Cup cup, Ball p){
		
		Ball nextNode = p.next;
		Ball prevNode = p.prev;

		//System.out.println("Pointer: " + p.value);

		//System.out.println(p.value + " deleted from " + cup.value);
			if(nextNode != null){
			nextNode.prev = prevNode;
			}
			
			if(prevNode != null){
			prevNode.next = nextNode;
			}
			
			if(p == this.front){
				this.front = nextNode;
			}
			
			if(p == this.rear){
				this.rear = prevNode;
			}

			p = p.next;

		}
	}



