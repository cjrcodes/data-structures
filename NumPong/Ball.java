//Ball, functions similar to a linked list node. This one works as a doubly linked list node, as there is a previous and a next.
public class Ball {

	int value;
	Ball next, prev;
	
	public Ball(int value){
		this.value = value;
		this.next = this.prev =  null;
	}
	
	public boolean stillContainsValue(Cup a){
		Ball b = this;
		
		while(b != null){
			if(b.value == a.value){
				return true;
			}
			
			else{
			System.out.println(b.value);
			b = b.next;
			}
		}
		
		return false;
	}
}
