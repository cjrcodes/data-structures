import java.util.TimerTask;

public class Queue extends TimerTask{

	QNode front, rear, ptr;
	String name;
	
	public Queue(){
		this.front = this.rear = null;
	}
	
	public Queue(String name){
		this.front = this.rear = null;
		this.name = name;
	}
	
	void enqueue(int key) 
    { 
          
        // Create a new LL node 
        QNode temp = new QNode(key); 
       
        // If queue is empty, then new node is front and rear both 
        if (this.rear == null) 
        { 
           System.out.println("Patron " + key + " has been enqueued");
           this.front = this.rear = temp; 
           return; 
        } 
       System.out.println("Patron " + key + " has been enqueued");
        // Add the new node at the end of queue and change rear 
        this.rear.next = temp; 
        this.rear = temp; 
    } 
       
    // Method to remove an key from queue.   
    QNode dequeue() 
    { 
        // If queue is empty, return NULL. 
        if (this.front == null){ 
 	       System.out.println("Empty Queue");
           return null; 
        }
       
        // Store previous front and move front one node ahead 
        QNode temp = this.front;
        this.front = this.front.next;
       
        // If front becomes NULL, then change rear also as NULL 

        System.out.println("Patron " + temp.key + " has been dequeued");

        if (this.front == null) 
           this.rear = null; 
        return temp; 
    }
    
    public static void printQueue(Queue q){
    	q.ptr = q.front;
    	
    	if(q.front == null){
    	       System.out.println("Empty Queue");
    	}
    	
    	while(q.ptr != null){
    		System.out.println(q.ptr.key);
    		q.ptr = q.ptr.next;
    	}
    }

	@Override
	public void run() {
		// TODO Auto-generated method stub
		
	}
    
    
} 
	 

