This is the Guessing Game!

---Data Structures: Binary Search, Arrays---

Goal: A random key number will be generated. The user must find the number out of 300 numbers.

Input: User will enter a guess number. If they get it wrong, they will be asked to enter another number to guess. This continues until they get it right.

Implementation: In order to help the user find the key faster, a binary search is used to find the lowest, middle, and highest valid numbers based on the user's last guess.



![Guessing Game Gif](Guessing Game/ezgif-4-f21ebdf76124.gif)
