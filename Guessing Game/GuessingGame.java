/* Program Name: Guessing Game

 * 
 * Purpose: Create a grid with a 12*25 dimension (300 elements) and allow the user to randomly guess
 * a number that is the key within the 300 elements and implement a binary search to help the user
 * find the key faster than a linear search.
 * 
 * Programmer: Christian Rodriguez
 * 
 */

import java.util.Random;
import java.util.Scanner;

public class GuessingGame {

	//Main method
	public static void main(String [] args) {
		//The grid size that will be created
		String [] arr = new String[300];
		System.out.println("\t\t\t\t///Christian's Guessing Game\\\\\\\n");
		createMatrix(assignMatrixValues(arr));
	}
	
	//Grid will be created here
	public static void createMatrix(String [] arr) {
		//This is a randomly generated key
		int key = generateKey();
		
		//Whether the key was found by the user
		boolean keyFound = false;
		
		//The current low value input by the user (1 by default)
		int low = 1;
		
		//The current high value input by the user (300 by default)
		int high = 300;
		
		//This saves the lowest value ever input by the user (1 by default)
		int lowest = 1;
		
		//This saves the highest value ever input by the user (300 by default)
		int highest = 300;

		//Initial print for the grid, currentIndex runs through the entire array.
		int currentIndex = 0;
		
		//Row
		for(int i = 0; i < 12; i++) {
			//Column
			for(int j = 0; j < 25; j++) {
				//These if/else statements will handle spacing
				if(currentIndex < 9) {
					System.out.print(arr[currentIndex] + "   ");
					currentIndex++;
				}
				
				else if(currentIndex >= 9 && currentIndex < 100) {
					System.out.print(arr[currentIndex] + "  ");
					currentIndex++;
				}
				
				else {
				System.out.print(arr[currentIndex] + " ");
				currentIndex++;
				}
			}
			System.out.println();
		}
		
		
		System.out.println("\n\n");
		System.out.println("Guess a number between 1 and 300: ");
		
		//Handles user input
		int input = userInput();
		
		//Key has not been found
		while(keyFound == false){
		
		//Key has been found	
		if(input == key){
			//Grid will be reprinted
			currentIndex = 0;
			for(int i = 0; i < 12; i++) {
				for(int j = 0; j < 25; j++) {
					if(currentIndex < 9) {
					if((currentIndex + 1) == key ){
						System.out.print(arr[currentIndex] + "   ");
						currentIndex++;
						}
						
						else{
						arr[currentIndex] = "X";
						System.out.print(arr[currentIndex] + "   ");
						currentIndex++;
						}
					}
					
					else if(currentIndex >= 9 && currentIndex < 100) {
						if((currentIndex + 1) == key){

							System.out.print(arr[currentIndex] + "  ");
							currentIndex++;
							}
							
							else{
							arr[currentIndex] = "X";
							System.out.print(arr[currentIndex] + "   ");
							currentIndex++;
							}
					}
					
					else {
						
						
							if((currentIndex + 1) == key){
								System.out.print(arr[currentIndex] + " ");
								currentIndex++;
								}
								
								else{
								arr[currentIndex] = "X";
								System.out.print(arr[currentIndex] + "   ");
								currentIndex++;
								}
					
					}
				}
				System.out.println();
			}
			keyFound = true;
			System.out.println("\nKey was found! " + key + " is the answer!");
			System.out.println("Press r and then Enter to restart the game. Press e and then Enter to exit.");

			menuInput();
			
		}
		
		//Input not equal to key
		else if(input != key){
			
			//Input lower than the key
			if(input < key){
				//Input lower than the lowest valid input ever by user
				if(input < lowest){
					System.out.println("\nYour guess is lower than your lowest valid guess, (" + lowest + "). Guess higher.");
					input = userInput();
					System.out.println();

				}
				
				//Input greater than lowest ever input
				else{
				low = input;
				lowest = (low + 1);
				//Middle value as guided by the binary search
				int mid = (high + low)/2;
				
				//If it is lower than or equal to the input, make it an "X"
				for(int i = 0; i <= low - 1; i++) {
					arr[i] = ("X");
					
					if(i == low - 1){
						currentIndex = 0;
					}
				}
				
				//Print grid with X's in appropriate locations
				for(int i = 0; i < 12; i++) {
					for(int j = 0; j < 25; j++) {
						if(currentIndex < 9 && currentIndex >= low - 1) {
							
							System.out.print(arr[currentIndex] + "   ");
							currentIndex++;
							
						}
						
						else if(currentIndex >= 9 && currentIndex < 100 && currentIndex >= low - 1) {
							if(arr[currentIndex].equals("X")){
								System.out.print(arr[currentIndex] + "   ");
								currentIndex++;
							}
							
							else{
							System.out.print(arr[currentIndex] + "  ");
							currentIndex++;
							}
						}
						
						else if(currentIndex >= 100 && currentIndex < 300 && currentIndex >= low - 1) {
							if(arr[currentIndex].equals("X")){
								System.out.print(arr[currentIndex] + "   ");
								currentIndex++;
							}
							else{
							System.out.print(arr[currentIndex] + " ");
							currentIndex++;
							}
						}
						
						else {
						System.out.print(arr[currentIndex] + "   ");
						currentIndex++;
						}
					}
					System.out.println();
				}
				
				System.out.println("\nKey was not found. You guessed lower than the key number. Try another number.");
				System.out.println("Maybe these will help you: ");
				System.out.println("Low guess: " + (low + 1) );
				System.out.println("Middle guess: " + mid);
				System.out.println("High guess: " + highest);

				input = userInput();
				
				System.out.println();
				}
			}
			
			//Input greater than key
			else{
				//Input higher than highest value ever input by the user
				if(input > highest){
					System.out.println(input);
					System.out.println("\nYour guess is higher than your highest valid guess,(" + highest + "). Guess lower.");
					input = userInput();
					System.out.println();

				}
				
				//Input lower or equal to highest ever valid input
				else{
				high = input;
				highest = (high - 1);
				int mid = (high + low)/2;
				
				//If it is higher than or equal to the input, make it an "X"

				for(int i = arr.length - 1; i >= highest; i--) {
					arr[i] = ("X");
					
					if(i == highest){
						currentIndex = 0;
					}
				}
				//Handles spacing and X's
				for(int i = 0; i < 12; i++) {
					for(int j = 0; j < 25; j++) {
						if(currentIndex < 9 && currentIndex <= high - 1) {
							System.out.print(arr[currentIndex] + "   ");
							currentIndex++;
						}
						
						else if(currentIndex >= 9 && currentIndex < 100 && currentIndex < high - 1) {
							if(arr[currentIndex].equals("X")){
								System.out.print(arr[currentIndex] + "   ");
								currentIndex++;
							}
							else{
							System.out.print(arr[currentIndex] + "  ");
							currentIndex++;
							}
						}
						
						else if(currentIndex >= 100 && currentIndex < 300 && currentIndex <= high - 1) {
							if(arr[currentIndex].equals("X")){
								System.out.print(arr[currentIndex] + "   ");
								currentIndex++;
							}
							else{
							System.out.print(arr[currentIndex] + " ");
							currentIndex++;
							}
						}
						
						else {
						System.out.print(arr[currentIndex] + "   ");
						currentIndex++;
						}
					}
					System.out.println();
				}

				System.out.println("\nKey was not found. You guessed higher than the key number. Try another number.");
				System.out.println("Maybe these will help you: ");
				System.out.println("Low guess: " + (lowest));
				System.out.println("Middle guess: " + mid);
				System.out.println("High guess: " + (high - 1));
				input = userInput();
				System.out.println();

				}
			}
			
		}
		}
	}
	
	//Assign all values in the matrix grid
	public static String [] assignMatrixValues(String [] arr) {
		for(int i = 0; i < arr.length; i++) {
			arr[i] = (Integer.toString((i) + 1));
		}
		
		return arr;
	}
	
	//Randomly generate a key
	public static int generateKey() {
		Random rand = new Random();
		int key = rand.nextInt(300) + 1;
		return key;
	}
	
	//Handles user input for restart and exiting
	public static int menuInput() {
		Scanner sc = new Scanner(System.in);
	    String input = sc.nextLine();
	   
	    if(!input.matches("[0-9]+")){
	    	
	    	//Exit
	    	if(input.equals("e")){
				System.out.println();
				System.out.println("Thanks for playing! :)");
	    		System.exit(0);
	    	}
	    	
	    	//Restart
	    	else if(input.equals("r")){
	    		String [] args = {"args"};
	    		main(args);
	    		
	    	}
	    	
	    	else{
	    		System.out.println("Input was neither e nor r. Please press(e)xit or (r)estart.");
	    		menuInput();
	    	}
	    }
	    
	    else{
	    	System.out.println("Input was neither e nor r. Please press(e)xit or (r)estart.");
    		menuInput();
    		}
	    
	    return -1;
		
}
	
	//Handles user input for the program
	public static int userInput() {
	Scanner sc = new Scanner(System.in);
    String input = sc.nextLine();
   
    //Checks if input is a number
    if(input.matches("[0-9]+")){
    //Checks for valid range
    if(Integer.parseInt(input) < 1 || Integer.parseInt(input) > 300) {
    	System.out.println("Invalid input, your input is out of range. Select a new number BETWEEN 1 and 300: ");
    	input = Integer.toString(userInput());
		System.out.println();

    }
    
    else{
    	return Integer.parseInt(input);
    }
    }
    
    //Checks if input is not a number
    else if(!input.matches("[0-9]+")){
    	System.out.println("Your input must be of the type integer (a number). Select a new number between 1 and 300: ");
    	input = Integer.toString(userInput());
		System.out.println();

    }
    
	return Integer.parseInt(input);

	}
}

//Thanks for reading my code! :)