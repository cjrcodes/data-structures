public class Stack {

	StackNode front, rear, ptr;
	String name;
	
	public Stack() {
		this.front = this.rear = null;
		this.name = null;
	}
	
	void Push(int key) {
		
		// Create a new LL node 
        StackNode temp = new StackNode(key); 
        
       
        // If stack is empty, then new node is front and rear both 
        if (this.rear == null) 
        { 
           //System.out.println("Patron " + key + " has been pushed");
           this.front = this.rear = temp;
           temp.diskPosition = 0;
           return; 
        } 
        
        else {
       //System.out.println("Patron " + key + " has been pushed");
        // Add the new node at the end of stack and change rear 
       StackNode oldFirst = this.front; 
       temp.diskPosition = oldFirst.diskPosition + 1;
    
       this.front = temp; 
       this.front.next = oldFirst;
       oldFirst.prev = temp;
        }
	}
	
	StackNode Pop() {
		 // If queue is empty, return NULL. 
        if (this.front == null){ 
 	       //System.out.println("Empty Stack");
           return null; 
        }
       
        // Store previous front and move front one node ahead 
        StackNode temp = this.front;
        this.front = this.front.next;
        
        
        /*this.ptr = this.front;
		
		while(this.ptr != null) {
			this.ptr.diskPosition++;
			this.ptr = this.ptr.next;
		}*/
        
       
        // If front becomes NULL, then change rear also as NULL 

       // System.out.println("Patron " + temp.key + " has been dequeued");

        if (this.front == null) 
           this.rear = null; 
        return temp; 
	}
	
		public static void printStack(Stack s) {
		s.ptr = s.front;
		
		System.out.println();
		if(s.front == null) {
			//System.out.println("Empty Stack");
		}
		
		while(s.ptr != null) {
			System.out.println(s.ptr.diskNum);
			s.ptr = s.ptr.next;
			
		}
	}
		
		public static int stackCounter(Stack s){
			s.ptr = s.front;
			
			int diskNum = 0;
			
			if(s.front == null){
				//Empty Stack
			}
			
			else{
				while(s.ptr != null) {
					diskNum++;
					s.ptr = s.ptr.next;
				}
				
				//System.out.println(diskNum);
			}
			
			return diskNum;
		}
}